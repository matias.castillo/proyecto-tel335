import { useEffect, useState } from 'react'
import Selector from '../components/selector.components'
import Body from '../components/body.components'

function ScrapperLayout () {

    const [ciudades, setCiudades] = useState([])

    console.log(ciudades)
    return(
        <div >
      <Selector setCiudades = {setCiudades} />
      <br></br>
      <Body ciudades = {ciudades}/>
    </div>
    )
}

export default ScrapperLayout