import React, {Component, useState} from 'react'
import Table from 'react-bootstrap/Table'
import DataTable from 'react-data-table-component'
import 'bootstrap/dist/css/bootstrap.min.css'
import Scrapper from './scrapper'


const columnas = [
  {
    name: 'ID',
    selector: 'id',
    sortable: true
  },
  {
    name: 'Ciudad',
    selector: 'nombre',
    sortable: true
  },
  {
    name: 'Temperatura',
    selector: 'temperatura',
    sortable: true
  },
  {
    name: 'Velocidad del viento',
    selector: 'viento',
    sortable: true
  },
  {
    name: 'Nubosidad',
    selector: 'nubosidad',
    sortable: true
  }
]

function Body (props) {

  const datos = []

  if(props){
    for(var i = 0; i<props.ciudades.length ; i++){
        let info = Scrapper(props.ciudades[i].URL, props.ciudades[i].Nombre, props.ciudades[i].id)
        datos.push(info)
    }
    
  }
  console.log("Datos body: ")
  console.log(datos)

  return(
    <div>
      <DataTable
      columns = {columnas}
      data = {datos}
      title = "Condiciones de ruta de vuelo"
      />
    </div>
  )
}

export default Body

/**
 *  Forma con Table, no probado en su totalidad

      <Table striped bordered hover className = "mt-3" >
            <thead>
                <tr>
                    <th>Ciudad</th>
                    <th>Temperatura</th>
                    <th>Velocidad del viento</th>
                    <th>Nubosidad</th>
                </tr>
            </thead>
            <tbody>
                {datos.map(item => (
                    <tr key={item.nombre} >    
                        <td>{item.nombre}</td>
                        <td>{item.temperatura}</td>
                        <td>{item.viento}</td>
                        <td >{item.nubosidad}</td>
                    </tr>
                ))}
            </tbody>
            </Table>
 * 
 */