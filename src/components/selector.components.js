import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import { useState } from 'react'

function Selector(props) {

    const [inicio, setInicio] = useState("")
    const [fin, setFin] = useState("")
    let ruta = []
    function Search(){
        if(inicio==="" || fin==="" ){
            alert("Por favor seleccione las comunas en las casillas")
        }
        const fetchData = async () => {
            
            let itemp
            let ftemp
            for(let i = 0; i < Ciudades.length ; i++ ){
                if(inicio===Ciudades[i].Nombre){
                    itemp = Ciudades[i].id
                    console.log("Id inicio: " + itemp)
                }
                else if(fin===Ciudades[i].Nombre){
                    ftemp = Ciudades[i].id
                    console.log("Id final: " + ftemp)
                }
            }
            if(itemp<ftemp){
                ruta = Ciudades.slice(itemp,ftemp+1)
                if(ruta){
                    props.setCiudades(ruta)
                }
            }
            else if(ftemp<itemp){
                let tempArray = Ciudades.slice(ftemp,itemp+1)
                ruta = tempArray.reverse();
                if(ruta){
                    props.setCiudades(ruta)
                }
            }
            console.log("Inicio: ", inicio, "Fin: ", fin)
        }
        fetchData()
    }
    return(
        <Form>
            <Form.Group controlId="exampleForm.SelectCustomSizeLg">
                <Form.Label>Inicio</Form.Label>
                <Form.Control as="select" size="sm" custom value={inicio} onChange={(value)=>setInicio(value.target.value)}>
                    <option>  </option>
                    <option>Arica</option>
                    <option>Iquique</option>
                    <option>Calama</option>
                    <option>Antofagasta</option>
                    <option>Caldera</option>
                    <option>La Serena</option>
                    <option>Santiago</option>
                    <option>Talcahuano</option>
                    <option>Freire</option>
                    <option>Valdivia</option>
                    <option>Osorno</option>
                    <option>Puerto Montt</option>
                    <option>Castro</option>
                    <option>Balmaceda</option>
                    <option>Natales</option>
                    <option>Punta Arenas</option>
                    <option>Isla de Pascua</option>


                </Form.Control>
            </Form.Group>
            <Form.Group controlId="exampleForm.SelectCustomSizeLg">
                <Form.Label>Fin</Form.Label>
                    <Form.Control as="select" size="sg" custom value={fin} onChange={(value)=>setFin(value.target.value)}>
                    <option>  </option>
                    <option>Arica</option>
                    <option>Iquique</option>
                    <option>Calama</option>
                    <option>Antofagasta</option>
                    <option>Caldera</option>
                    <option>La Serena</option>
                    <option>Santiago</option>
                    <option>Talcahuano</option>
                    <option>Freire</option>
                    <option>Valdivia</option>
                    <option>Osorno</option>
                    <option>Puerto Montt</option>
                    <option>Castro</option>
                    <option>Balmaceda</option>
                    <option>Natales</option>
                    <option>Punta Arenas</option>
                    <option>Isla de Pascua</option>
                    </Form.Control>
            </Form.Group>
            <Button variant="outline-info" onClick={ Search }>Buscar</Button>
        </Form>
    )
}

export default Selector

const Ciudades = [
    {
        id: 0,
        Nombre: "Arica",
        URL: "https://www.meteored.cl/tiempo-en_Arica-America+Sur-Chile-Tarapaca-SCAR-1-18563.html"
    },
    {
        id: 1,
        Nombre: "Iquique",
        URL: "https://www.meteored.cl/tiempo-en_Iquique-America+Sur-Chile-Tarapaca-SCDA-1-18257.html"
    },
    {
        id: 2,
        Nombre: "Calama",
        URL: "https://www.meteored.cl/tiempo-en_Calama-America+Sur-Chile-Antofagasta-SCCF-1-18564.html"
    },
    {
        id: 3,
        Nombre: "Antofagasta",
        URL: "https://www.meteored.cl/tiempo-en_Antofagasta-America+Sur-Chile-Antofagasta--1-135102.html"
    },
    {
        id: 4,
        Nombre: "Caldera",
        URL: "https://www.meteored.cl/tiempo-en_Caldera-America+Sur-Chile-Atacama--1-18565.html"
    },
    {
        id: 5,
        Nombre: "La Serena",
        URL: "https://www.meteored.cl/tiempo-en_La+Serena-America+Sur-Chile-Coquimbo-SCSE-1-18575.html"
    },
    {
        id: 6,
        Nombre: "Santiago",
        URL: "https://www.meteored.cl/tiempo-en_Santiago+de+Chile-America+Sur-Chile-Region+Metropolitana+de+Santiago-SCEL-1-18578.html"
    },
    {
        id: 7,
        Nombre: "Talcahuano",
        URL: "https://www.meteored.cl/tiempo-en_Talcahuano-America+Sur-Chile-Biobio--1-18265.html"
    },
    {
        id: 8,
        Nombre: "Freire",
        URL: "https://www.meteored.cl/tiempo-en_Freire-America+Sur-Chile-Araucania--1-18148.html"
    },
    {
        id: 9,
        Nombre: "Valdivia",
        URL: "https://www.meteored.cl/tiempo-en_Valdivia-America+Sur-Chile-Los+Lagos--1-18266.html"
    },
    {
        id: 10,
        Nombre: "Osorno",
        URL: "https://www.meteored.cl/tiempo-en_Osorno-America+Sur-Chile-Los+Lagos-SCJO-1-18258.html"
    },
    {
        id: 11,
        Nombre: "Puerto Montt",
        URL: "https://www.meteored.cl/tiempo-en_Puerto+Montt-America+Sur-Chile-Los+Lagos-SCTE-1-18567.html"
    },
    {
        id: 12,
        Nombre: "Castro",
        URL: "https://www.meteored.cl/tiempo-en_Castro-America+Sur-Chile-Los+Lagos--1-18183.html"
    },
    {
        id: 13,
        Nombre: "Balmaceda",
        URL: "https://www.meteored.cl/tiempo-en_Balmaceda-America+Sur-Chile-Aisen+del+General+Carlos+Ibanez+del+Campo--1-516060.html"
    },
    {
        id: 14,
        Nombre: "Natales",
        URL: "https://www.meteored.cl/tiempo-en_Puerto+Natales-America+Sur-Chile-Magallanes+y+de+la+Antartica+Chilena--1-18201.html"
    },
    {
        id: 15,
        Nombre: "Punta Arenas",
        URL: "https://www.meteored.cl/tiempo-en_Punta+Arenas-America+Sur-Chile-Magallanes+y+de+la+Antartica+Chilena-SCCI-1-18570.html"
    },
    {
        id: 16,
        Nombre: "Isla de Pascua",
        URL: "https://www.meteored.cl/tiempo-en_Isla+de+Pascua-America+Sur-Chile-Isla+de+Pascua--1-222169.html"
    }
        
]

