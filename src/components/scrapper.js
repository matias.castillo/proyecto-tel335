import rp from 'request-promise'
import cheerio from 'cheerio'

function Scrapper ( URL , nombre, id) {

    let datos = new Object

    rp("https://cors-anywhere.herokuapp.com/"+URL)
      .then(html => {
        let $ = cheerio.load(html);
        let temp = $("span.dato-temperatura")
        let viento = $("body > span.franjas > span.columnas.zona-contenido.padding-top-doble.horas-sol-lunas > span > span:nth-child(1) > section > table > tbody > tr:nth-child(1) > td:nth-child(7) > span > span.datos-viento > span:nth-child(3)")
        let infoT = $("body > span.franjas > span.columnas.zona-contenido.padding-top-doble.horas-sol-lunas > span > span:nth-child(1) > section > table > tbody > tr:nth-child(1) > td.descripcion > strong")

        datos.id = id
        datos.nombre = nombre
        datos.temperatura = temp.text()
        datos.viento = viento.text()
        datos.nubosidad = infoT.text()
        
      })
      .catch(function(err) {
        console.log("get failed");
      })

      return datos

}

export default Scrapper
