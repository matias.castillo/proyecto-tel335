import logo from './logo.svg';
import './App.css';
import ScrapperLayout from './layout/scrapper.layout'


function App(){

  return (
    <div >
      <ScrapperLayout />
    </div>
  );
}

export default App;

